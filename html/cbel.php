<div class="row">
    <div class="col-md-1">
        <a class="btn btn-default btn-md" href="index.php#portfolio">Back to Portfolio</a>
    </div>
</div>

<!-- IMAGE GALLERY -->
<!--===============================================================-->
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <?php
            require_once('html/cbel_image_gallery.php');
        ?>
    </div>
</div>  

<br />

<!-- TECHNOLOGIES -->
<!--===============================================================-->
<div class="row">
    <div class="col-md-12">
        <h2>Languages and Frameworks Used</h2>
    </div>
</div>

<div class="row">
    <div class="col-sm-2 padding">
        <div class="vcenter">
            <img src="assets/images/bootstrap.jpg" width="36" height="36" alt="Bootstrap icon" />
        </div>
        <div class="vcenter">
            Bootstrap
        </div>
    </div>

    <div class="col-sm-2">
        <div class="vcenter">                
            <i class="fa fa-html5 fa-3x"></i>
        </div>
        <div class="vcenter">
            HTML5
        </div>
    </div>

    <div class="col-sm-2">
        <div class="vcenter">
            <i class="fa fa-css3 fa-3x"></i>
        </div>
        <div class="vcenter">
            CSS3
        </div>
    </div>

    <div class="col-sm-2 padding">
        <div class="vcenter">
            <img src="assets/images/javascript.png" width="34" height="38" alt="JavaScript icon" />
        </div>
        <div class="vcenter">
            JavaScript
        </div>
    </div>

    <div class="col-sm-2 extra-padding">
        <div class="vcenter">
            <img src="assets/images/php.png" width="76" height="38" alt="PHP icon" />
        </div>
        <div class="vcenter">
            PHP
        </div>
    </div>

    <div class="col-sm-2 extra-padding">
        <div class="vcenter">
            <img src="assets/images/mysql.png" width="38" height="38" alt="MySQL icon" />
        </div>
        <div class="vcenter">
            SQL/MYSQL
        </div>
    </div>
</div>

<br />

<!-- SUMMARY AND REFLECTION -->
<!--===============================================================-->
<div class="row">
    <div class="col-md-12">    
        <h2>Summary</h2>

        <div class="row">
            <div class="col-md-12">
                <h4>Time to Develop: March 2014 - April 2014</h4>
            </div>
        </div>

        <br />

        <div class="row">
            <div class="col-md-10">
                <p class="text">
                    CBEL Tracker was built as a part of CPSC 319: Software Engineering Project. We were required 
                    to form into development teams of six and then assign ourselves roles that would commonly 
                    appear in an actual devolpment team. Each team member also had to claim leadership of a particular 
                    phase in the waterfall model of software development.  My roles were Project Manager and Web Master, 
                    and I led the Coding phase.
                </p>

                <br />

                <p class="text">
                    After choosing our teams and roles we were assigned a client that we would work with for the 
                    remainder of the term.  All the clients were actual organizations looking for the development of 
                    software.  Each client worked with four different teams, so we were also competing against each 
                    other to be the chosen team.  Our client was the UBC Center for Community Engaged Learning (CCEL).
                </p>

                <br />

                <p class="text">
                    CCEL is an organization within UBC that "collaborates with students, staff, faculty and community 
                    partners to work through complex community-based issues, both locally and internationally" 
                    (<a href="http://students.ubc.ca/about/centre-community-engaged-learning">CCEL Site</a>)They were 
                    looking for a web-based application that they could use internally to keep track of and develop 
                    Community-Based Experiential Learning(CBEL) Leads.  These were essentially ideas for ways CCEL could 
                    connect various parties for community services.  At the time CCEL was usingan Excel spreadsheet to 
                    keep track of CBEL Leads, which they found was difficult to scan for a particular lead and easy to 
                    forget what leads werein progress.  They also found it inefficient to share leads with others, as it 
                    required both people being in the CCEL office or creating an emailwith the details.
                </p>

                <br />

                <p class="text">
                    Our solution for CCEL provided the following functions:

                    <ul>
                        <li>Access restrictions through user accounts</li>
                        <li>
                            Home page showing New Leads, Urgent Leads (Leads with a due date within the next 7 days), and 
                            Popular Leads (top 6 Leads with user activity) 
                        </li>
                        <li>Creation, editing, and deletion of Leads</li>
                        <li>Automatic filling in of Community Partner information from a select list</li>
                        <li>More user-friendly multi-select boxes with checkboxes</li>
                        <li>   
                            Exporting a Lead or Community Partner to a CSV file for importing into the other UBC system 
                            CCEL uses
                        </li>
                        <li>Linking a Lead to another Lead to show they are related</li>
                        <li>Tagging a Lead to receive internal and email notifications when it has been updated</li>
                        <li>Commenting on a Lead</li>
                        <li>Comprehensive search for Leads</li>
                        <li>Viewing of statistics regarding successful, attempted, and failed Leads</li>
                    </ul>
                </p>
            </div>
        </div>

        <h2>Functions Responsible For</h2>

        <div class="row">
            <div class="col-md-10">
                <p class="text">
                    <ul>
                        <li>Layout and navbar of whole application</li>
                        <li>Lead creation/editing/deletion</li>
                        <li>Lead search and results</li>
                        <li>CSV exporting</li>
                    </ul>
                </p>
            </div>
        </div>

        <h2>Reflection</h2>

        <div class="row">
            <div class="col-md-10">
                <p class="text">
                    CBEL Tracker was my first real web development project.  Previously, I only had minor exposure to 
                    HTML/CSS and PHP through typical school assignments. Working on the project also gave me a much greater 
                    understanding of how software is actually developed, from conception to testing.  All my other projects 
                    had been pre-determined by my professors on what they would do, but for this project I got to decide what 
                    it would do and how it would do it.  This was also my first time creating documentation for my software, 
                    as we had to create a Software Requirements Specification, Design Document, User Manual, and Testing Plan.  
                    Also, unlike previous projects, this one required significant planning and discussion.  We had to have
                    multiple meetings with our client and make various drafts of the SRS and get it approved before developing 
                    the system.
                </p>

                <br />

                <p class="text">
                    My roles of Project Manager and Coding Leader allowed me to showcase and further develop my leadership and 
                    teaching abilities.  I was the one responsible for assigning tasks and ensuring they were completed.  There 
                    were difficulties in this, as only one other team member had any experience with PHP and only two others had 
                    any experience with MySQL and databases.  As I was the only one with experience with both and using them 
                    together, I spent a significant  amount of time helping my teammates solve their coding problems.  Furthermore, 
                    there was also a language barrier with one teammate and the sixth dropped the course early on, leaving us one 
                    member short.
                </p>
            </div>
        </div>

        <br />

        <h2>Additional Resources</h2>

        <div class="row">
            <div class="col-md-10">
                <h4>
                    <a href="https://sites.google.com/site/cs319w13team8/home" target="_blank">Team Site</a>
                </h4>

                <h4>
                    <a href="https://bitbucket.org/kharrison1421/code-visualizer.git" target="_blank">Code Repository</a>
                </h4>
            </div>
        </div>

        <br />

        <div class="row">
            <div class="col-md-1">
                <a class="btn btn-default btn-md" href="index.php#portfolio">Back to Portfolio</a>
            </div>
        </div>
    </div>
</div>