<!-- IMAGE GALLERY -->
<!--===============================================================-->
<!-- Button trigger modal -->
<a class="wrapper-portfolio large-portfolio-image" data-toggle="modal" data-target="#cbel">
    <img class="img-responsive" src="assets/images/cbel/home.png" alt="Image gallery">
    <h3 class="category">Image Gallery</h3>
</a>


<!-- Modal -->
<div class="modal fade" id="cbel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">CBEL Tracker</h4>
            </div>
            <div class="modal-body">
                <!-- Picture carousel -->
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img src="assets/images/cbel/login.png" alt"Login">
                            <div class="carousel-caption">
                                Login Page
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/cbel/signup.png" alt"Signup">
                            <div class="carousel-caption">
                                Signup Page
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/cbel/home.png" alt"Home Page">
                            <div class="carousel-caption">
                                Home Page
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/cbel/edit_lead.png" alt"Lead Edit">
                            <div class="carousel-caption">
                                Lead Edit Page
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/cbel/comments.png" alt"Lead Comments">
                            <div class="carousel-caption">
                                Lead Comments
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/cbel/link.png" alt"Lead Links">
                            <div class="carousel-caption">
                                Lead Links
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/cbel/lead_export.png" alt"Export Lead">
                            <div class="carousel-caption">
                                Export Lead
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/cbel/search.png" alt"Lead Search">
                            <div class="carousel-caption">
                                Lead Search Page
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/cbel/notifications.png" alt"Notifications">
                            <div class="carousel-caption">
                                Notifications Page
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/cbel/profile.png" alt"Profile">
                            <div class="carousel-caption">
                                Settings Page
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/cbel/accounts.png" alt"Accounts">
                            <div class="carousel-caption">
                                Accounts Tab
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/cbel/categories.png" alt"Categories">
                            <div class="carousel-caption">
                                Categories Tab
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/cbel/statistics.png" alt"Statistics">
                            <div class="carousel-caption">
                                Statistics Tab
                            </div>
                        </div>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-bottom:0">Close</button>
            </div>
        </div>
    </div>
</div>
<!--===============================================================-->