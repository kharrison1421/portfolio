<div class="row">
    <div class="col-md-1">
        <a class="btn btn-default btn-md" href="index.php#portfolio">Back to Portfolio</a>
    </div>
</div>
        
<!-- IMAGE GALLERY -->
<!--===============================================================-->
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <?php
            require_once('html/flex_image_gallery.php');
        ?>
    </div>
</div>  

<br />

<!-- TECHNOLOGIES -->
<!--===============================================================-->
<div class="row">
    <div class="col-md-12">
        <h2>Languages and Frameworks Used</h2>
    </div>
</div>

<div class="row">
    <div class="col-sm-2 padding">
        <div class="vcenter">
            <img src="assets/images/bootstrap.jpg" width="36" height="36" alt="Bootstrap icon" />
        </div>
        <div class="vcenter">
            Bootstrap
        </div>
    </div>

    <div class="col-sm-2">
        <div class="vcenter">                
            <i class="fa fa-html5 fa-3x"></i>
        </div>
        <div class="vcenter">
            HTML5
        </div>
    </div>

    <div class="col-sm-2">
        <div class="vcenter">
            <i class="fa fa-css3 fa-3x"></i>
        </div>
        <div class="vcenter">
            CSS3
        </div>
    </div>

    <div class="col-sm-2 padding">
        <div class="vcenter">
            <img src="assets/images/javascript.png" width="34" height="38" alt="JavaScript icon" />
        </div>
        <div class="vcenter">
            JavaScript
        </div>
    </div>
</div>

<br />

<div class="row">
    <div class="col-sm-2 padding">
        <div class="vcenter">
            <img src="assets/images/laravel.png" width="57" height="38" alt="Laravel icon" />
        </div>
        <div class="vcenter">
            Laravel
        </div>
    </div>

    <div class="col-sm-2 extra-padding">
        <div class="vcenter">
            <img src="assets/images/php.png" width="76" height="38" alt="PHP icon" />
        </div>
        <div class="vcenter">
            PHP
        </div>
    </div>

    <div class="col-sm-2 extra-padding">
        <div class="vcenter">
            <img src="assets/images/mysql.png" width="38" height="38" alt="MySQL icon" />
        </div>
        <div class="vcenter">
            SQL/MYSQL
        </div>
    </div>
</div>

<br />

<!-- SUMMARY AND REFLECTION -->
<!--===============================================================-->
<div class="row">
    <div class="col-md-12">    
        <h2>Summary</h2>

        <div class="row">
            <div class="col-md-12">
                <h4>Time to Develop: August 2014 - March 2015</h4>
            </div>
        </div>

        <br />

        <div class="row">
            <div class="col-md-10">
                <p class="text">
                    Freightlink Express is a small trucking company that ships freight between major airports throughout Canada and the US.  
                    Previously they had been handling all orders through the creation of paper documents, which became tedious and inefficient 
                    as business increased.  In May 2014 I was contacted to develop a web-based system for them: FLEX.
                </p>

                <br />

                <p class="text">
                    Three types of documents are primarily used in the creation of a trucking order.  The Probill is the order itself and includes
                    information such as shipper, consignee, origin, destination, and date, as well as a list of Air Waybills detailing the items in 
                    the shipment.  Each Probill has an Invoice for the customer to be billed.  Once a shipment is ready for departure it is placed
                    on a Line Manifest, detailing all the shipments on a particular truck going from one airport to another. 
                </p>

                <br />

                <p class="text">
                    FLEX performs the following functions:

                    <ul>
                        <li>User access restrictions through user accounts</li>
                        <li>Displays list and summary of Probills not yet on a Line Manifest</li>
                        <li>Creation, editing, and deletion of Probill and Line Manifest forms</li>
                        <li>More user-friendly multi-select boxes with checkboxes</li>
                        <li>Automatic population of customer data from list of existing customers</li>
                        <li>Automatic generation of an Invoice for every Probill</li>
                        <li>Automatic population of Probill information when creating a Line Manifest</li>
                        <li>Display Probill, Invoice, and Line Manifest as PDFs</li>
                        <li>'Soft deletion' of all forms; deleted forms can be recovered.</li>
                        <li>Comprehensive search and cross-referencing for Probills and Line Manifests</li>
                        <li>Scan and upload documents into FLEX using a TWAIN compatable scanner</li>
                        <li>Generation of invoice logs for customers for user specified date ranges</li>
                    </ul>
                </p>
            </div>
        </div>

        <h2>Reflection</h2>

        <div class="row">
            <div class="col-md-10">
                <p class="text">
                    Working on FLEX really strengthend my skills regarding web development, server administration, document creation, and customer
                    interaction.  Through working with the Laravel framework I gained a stronger grasp on MVC architecture and the benefits of using
                    a framework, including much neater, better structured, and more efficient code.  As I was responsible for the migration of the 
                    system to a live server, I also gained a good understanding of the different types of servers, along with security measures for the 
                    files on the server.  Finally, I also increased my ability to write comprehensive and professional documents, as I had to create
                    a Software Requirements Specification document, contract, design document, and user's manual.
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-1">
                <a class="btn btn-default btn-md" href="index.php#portfolio">Back to Portfolio</a>
            </div>
        </div>
    </div>
</div>