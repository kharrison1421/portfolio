<!-- IMAGE GALLERY -->
<!--===============================================================-->
<!-- Button trigger modal -->
<a class="wrapper-portfolio large-portfolio-image" data-toggle="modal" data-target="#flex">
    <img class="img-responsive" src="assets/images/flex/pending.png" alt="Image gallery">
    <h3 class="category">Image Gallery</h3>
</a>


<!-- Modal -->
<div class="modal fade" id="flex" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">FLEX</h4>
            </div>
            <div class="modal-body">
                <!-- Picture carousel -->
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img src="assets/images/flex/login.png" alt"Login Page">
                            <div class="carousel-caption">
                                Login Page
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/flex/pending.png" alt="Pending Probills">
                            <div class="carousel-caption">
                                Pending Probills
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/flex/probill_edit.png" alt="Probill Edit">
                            <div class="carousel-caption">
                                Probill Edit
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/flex/probill_view.png" alt="Probill View">
                            <div class="carousel-caption">
                                Probill View
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/flex/probill_print.png" alt="Probill PDF">
                            <div class="carousel-caption">
                                Probill PDF
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/flex/invoice.png" alt="Invoice">
                            <div class="carousel-caption">
                                Invoice
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/flex/upload_documents.png" alt="Upload Documents">
                            <div class="carousel-caption">
                                Upload Documents
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/flex/manifest_edit.png" alt="Line Manifest Edit">
                            <div class="carousel-caption">
                                Line Manifest Edit
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/flex/manifest_print.png" alt="Line Manifest Print">
                            <div class="carousel-caption">
                                Line Manifest Print
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/flex/search.png" alt="Probill Search">
                            <div class="carousel-caption">
                                Probill Search
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/flex/search_results.png" alt="Search Results">
                            <div class="carousel-caption">
                                Search Results
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/flex/customer.png" alt="Customers">
                            <div class="carousel-caption">
                                Customers
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/flex/invoice_log.png" alt="Invoice Log">
                            <div class="carousel-caption">
                                Invoice Log
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/flex/users.png" alt="Users">
                            <div class="carousel-caption">
                                Users
                            </div>
                        </div>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-bottom:0">Close</button>
            </div>
        </div>
    </div>
</div>
<!--===============================================================-->