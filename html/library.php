<div class="row">
    <div class="col-md-1">
        <a class="btn btn-default btn-md" href="index.php#portfolio">Back to Portfolio</a>
    </div>
</div>
        
<!-- IMAGE GALLERY -->
<!--===============================================================-->
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <?php
            require_once('html/library_image_gallery.php');
        ?>
    </div>
</div>  

<br />

<!-- TECHNOLOGIES -->
<!--===============================================================-->
<div class="row">
    <div class="col-md-12">
        <h2>Languages and Frameworks Used</h2>
    </div>
</div>

<div class="row">
    <div class="col-sm-2">
        <div class="vcenter">
            <img src="assets/images/java.png" width="40" height="40" alt="JavaScript icon" />
        </div>
        <div class="vcenter">
            Java
        </div>
    </div>

    <div class="col-sm-2 extra-padding">
        <div class="vcenter">
            <img src="assets/images/oracle.png" width="38" height="38" alt="Oracle icon" />
        </div>
        <div class="vcenter">
            SQL/Oracle
        </div>
    </div>
</div>

<br />

<!-- SUMMARY AND REFLECTION -->
<!--===============================================================-->
<div class="row">
    <div class="col-md-12">    
        <h2>Summary</h2>

        <div class="row">
            <div class="col-md-12">
                <h4>Time to Develop: March 2013 - April 2013</h4>
            </div>
        </div>

        <br />

        <div class="row">
            <div class="col-md-10">
                <p class="text">
                    The Mock Library was developed as a project for CPSC 304: Introduction to Relational Databases.  We were
                    required to create an application in groups of four that would mimic actions commonly performed in a library.  
                    Said actions were already predetermined; we just had to implement them.  We were also required to use Java, SQL,
                    and an Oracle database.  
                </p>

                <br />

                <p class="text">
                    The program was not required to be robust, have input validation, or even have a proper GUI, but we were allowed 
                    if we wanted to; it only had to perform the required actions and have the expected results. As my team wanted more 
                    practice with Java and to produce something that looked good, we the application does have some robustness and 
                    input validation, as well as a GUI.
                </p>

                <br />

                <p class="text">
                    Since each student in the course was given an account for an Oracle database, which was then deactivated after 
                    the course ended, and the project made use of one of them, some functions no longer work. For the functions that 
                    no longer work, a dialog box or table with results would show up, depending on the action.
                </p>

                <br />

                <p class="text">
                    The application was required to have three types of users, each with their own set of functions.  The Mock Library 
                    performs the following actions:
                    
                    <div class="row">
                        <div class="col-md-4">
                            <h4>Clerks:</h4>
                            <ul>
                                <li>Add Borrower</li>
                                <li>Checkout books</li>
                                <li>Return books</li>
                                <li>Display a list of overdue books</li>
                                <li>Send emails (just a popup) informing of overdue books</li>
                            </ul>
                        </div>

                        <div class="col-md-4">
                            <h4>Borrowers:</h4>
                            <ul>
                                <li>Login (obtains Borrower id)</li>
                                <li>Search for books</li>
                                <li>Request a hold on a book</li>
                                <li>View list of checked out books</li>
                                <li>View any fines</li>
                                <li>Pay fines</li>
                            </ul>
                        </div>

                        <div class="col-md-4">
                            <h4>Librarians:</h4>
                            <ul>
                                <li>Add a book/copies to database</li>
                                <li>Display a list of all checked out books</li>
                                <li>Display a list of the most popular books</li>
                            </ul>
                        </div>
                    </div>
                </p>

                <p class="text">
                    Behind the scenes, books were assigned due dates based on the type of borrower: Student, Faculty, or Staff.  Books
                    still checkout out after the due date accumulated daily fines.  Also, the system ensured the same copy of a book
                    couldn't be checkout out if another user already checked it out.
                </p>
            </div>
        </div>

        <h2>Functions Responsible For</h2>

        <div class="row">
            <div class="col-md-10">
                <p class="text">
                    <ul>
                        <li>Book search</li>
                        <li>Add book</li>
                        <li>Checkout book</li>
                        <li>Overdue books list</li>
                        <li>Overdue books emails</li>
                    </ul>
                </p>
            </div>
        </div>

        <h2>Reflection</h2>

        <div class="row">
            <div class="col-md-10">
                <p class="text">
                    This project was my first real use of SQL.  I'd learned the basics of SQL before, but had never used it beyond typical
                    school assignments and exercises.  Working on the Mock Library solidified what I'd learned previously and through the
                    course regarding SQL and RDBMSs.  My team received an A+ on the project.
                </p>
            </div>
        </div>

        <br />

        <h2>Additional Resources</h2>

        <div class="row">
            <div class="col-md-10">
                <h4>
                    <a href="https://kharrison1421@bitbucket.org/kharrison1421/mock-library.git" target="_blank">Code Repository</a>
                </h4>
            </div>
        </div>

        <br />

        <div class="row">
            <div class="col-md-1">
                <a class="btn btn-default btn-md" href="index.php#portfolio">Back to Portfolio</a>
            </div>
        </div>
    </div>
</div>