<!-- IMAGE GALLERY -->
<!--===============================================================-->
<!-- Button trigger modal -->
<a class="wrapper-portfolio large-portfolio-image" data-toggle="modal" data-target="#library">
    <img class="img-responsive" src="assets/images/library/library.png" alt="Image gallery">
    <h3 class="category">Image Gallery</h3>
</a>


<!-- Modal -->
<div class="modal fade" id="library" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Mock Library</h4>
            </div>
            <div class="modal-body">
                <!-- Picture carousel -->
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img src="assets/images/library/select_user.png" alt"Select User Type">
                            <div class="carousel-caption library-caption">
                                Select User Type
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/library/add_borrower.png" alt"Clerk Add Borrower">
                            <div class="carousel-caption library-caption">
                                Clerk Add Borrower
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/library/checkout.png" alt"Clerk Checkout Book">
                            <div class="carousel-caption library-caption">
                                Clerk Checkout Book
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/library/return.png" alt"Clerk Return Book">
                            <div class="carousel-caption library-caption">
                                Clerk Return Book
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/library/overdue.png" alt"lerk Check Overdue Items">
                            <div class="carousel-caption library-caption">
                                Clerk Check Overdue Items
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/library/login.png" alt"Borrower Login">
                            <div class="carousel-caption library-caption">
                                Borrower Login
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/library/search.png" alt"Borrower Book Search">
                            <div class="carousel-caption library-caption">
                                Borrower Book Search
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/library/hold.png" alt"Borrower Hold Request">
                            <div class="carousel-caption library-caption">
                                Borrower Hold Request
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/library/account.png" alt"Borrower Check Account">
                            <div class="carousel-caption library-caption">
                                Borrower Check Account
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/library/fine.png" alt"Borrower Pay Fine">
                            <div class="carousel-caption library-caption">
                                Borrower Pay Fine
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/library/add_book.png" alt"Librarian Add Book">
                            <div class="carousel-caption library-caption">
                                Librarian Add Book
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/library/book_report.png" alt"Librarian Book Report">
                            <div class="carousel-caption library-caption">
                                Librarian Book Report
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/library/popular.png" alt"Librarian Popular Items Report">
                            <div class="carousel-caption library-caption">
                                Librarian Popular Items Report
                            </div>
                        </div>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-bottom:0">Close</button>
            </div>
        </div>
    </div>
</div>
<!--===============================================================-->