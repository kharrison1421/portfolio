<div class="row">
    <div class="col-md-1">
        <a class="btn btn-default btn-md" href="index.php#portfolio">Back to Portfolio</a>
    </div>
</div>
        
<!-- IMAGE GALLERY -->
<!--===============================================================-->
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <?php
            require_once('html/visualizer_image_gallery.php');
        ?>
    </div>
</div>  

<br />

<!-- TECHNOLOGIES -->
<!--===============================================================-->
<div class="row">
    <div class="col-md-12">
        <h2>Languages and Frameworks Used</h2>
    </div>
</div>

<div class="row">
    <div class="col-sm-2">
        <div class="vcenter">
            <img src="assets/images/java.png" width="40" height="40" alt="JavaScript icon" />
        </div>
        <div class="vcenter">
            Java
        </div>
    </div>
</div>

<br />

<!-- SUMMARY AND REFLECTION -->
<!--===============================================================-->
<div class="row">
    <div class="col-md-12">    
        <h2>Summary</h2>

        <div class="row">
            <div class="col-md-12">
                <h4>Time to Develop: October 2013 - November 2013</h4>
            </div>
        </div>

        <br />

        <div class="row">
            <div class="col-md-10">
                <p class="text">
                    The Robotanism Flower Visualizer was a project created in CPSC 410: Advanced Software Engineering.  In teams of 
                    four, we were required to create a tool that analysed existing code and visualized some aspect of it.  My team
                    decided to interpret Java code into flowers.
                </p>

                <br />

                <p class="text">
                    Using our tool, a number of facts can be observed.  Each flower represents a Java class and each class is grouped
                    into a coloured box representing its package.  The height of a flower is based on how many lines of code are in the
                    class, with the width increase proportionally.  Each petal on a flower represents a method and each root represents
                    an import.  Finally, each arrow represents one class calling a method from another class, with thickness increasing
                    for each method call.
                </p>

                <br />

                <p class="text">
                    Once the flowers have been generated and displayed, one can look for a number of code smells.  For instance, a flower
                    that towers over the other ones is likely a sign of a class that is too big.  This could mean that the class is trying
                    to do too many different things, signifying the presence of a God object and thus having low cohesion.  It could also 
                    indicate that the code is written inefficently. On the other end of the spectrum, a flower that is much smaller than
                    the others could indicate a lazy class.  Another code smell that can be easily seen is high coupling.  If an arrow is 
                    really thick between two classes it means that one of the classes is heavily dependent on the other, possibly indicating
                    high coupling, feature envy, or inappropriate intimacy.  Also, if a class has arrows pointing to many different classes 
                    it could mean that the class is dependent on too many classes. 
                </p>

                <p class="text">
                    In April 2015, to refresh my Java skills and learn about using RMI for distributed computing, I incorporated the visualizer
                    tool into an RMI project. In this version there are three parts: the client, the compute interface, and the compute engine.  
                    The client can be on a client computer and the engine can be on a server.  The compute engine is required by both the client 
                    and the engine and the engine must be running before the client can interact with it.  The Parser and Translator both run
                    on the server, while the Visualizer runs on the client.
                </p>
            </div>
        </div>

        <h2>Functions Responsible For</h2>

        <div class="row">
            <div class="col-md-10">
                <p class="text">
                    <ul>
                        <li>Locating Java files in given directory</li>
                        <li>Parsing Java files</li>
                        <li>Creating objects with all necessary data</li>
                    </ul>
                </p>
            </div>
        </div>

        <h2>Reflection</h2>

        <div class="row">
            <div class="col-md-10">
                <p class="text">
                    This project was interesting because it allowed me to evaluate previous projects I'd worked on and compare them to my
                    current abilities.  The images I've provided above are screenshots of the results after running the Visualizer on three
                    different projects. It can be seen that in two projects there were God Objects and one with high coupling.  Contrast that
                    with the image of the Visualizer analysing itself and it can be seen that I had learned from my previous projects, as the
                    Visualizer has acceptable class sizes, cohesion, and coupling.
                </p>
            </div>
        </div>

        <br />

        <h2>Additional Resources</h2>

        <div class="row">
            <div class="col-md-10">
                <h4>
                    <a href="https://kharrison1421@bitbucket.org/kharrison1421/code-visualizer.git" target="_blank">Code Repository</a>
                </h4>

                <h4>
                    <a href="https://kharrison1421@bitbucket.org/kharrison1421/rmi-code-visualizer.git" target="_blank">Code Repository (RMI Version)</a>
                </h4>

                <h4>
                    <a href="assets/documents/Proposal and Prototype.pdf" target="_blank">Proposal and Prototype</a>
                </h4>

                <h4>
                    <a href="assets/documents/Tool Architecture.pdf" target="_blank">Tool Architecture</a>
                </h4>

                <h4>
                    <a href="assets/documents/Final Writeup.pdf" target="_blank">Final Writeup</a>
                </h4>
            </div>
        </div>

        <br />

        <div class="row">
            <div class="col-md-1">
                <a class="btn btn-default btn-md" href="index.php#portfolio">Back to Portfolio</a>
            </div>
        </div>
    </div>
</div>