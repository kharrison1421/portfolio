<!-- IMAGE GALLERY -->
<!--===============================================================-->
<!-- Button trigger modal -->
<a class="wrapper-portfolio large-portfolio-image" data-toggle="modal" data-target="#visualizer">
    <img class="img-responsive" src="assets/images/visualizer/flowers.png" alt="Image gallery">
    <h3 class="category">Image Gallery</h3>
</a>


<!-- Modal -->
<div class="modal fade" id="visualizer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Robotanism Code Visualizer</h4>
            </div>
            <div class="modal-body">
                <!-- Picture carousel -->
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img src="assets/images/visualizer/flowers.png" alt"Pipieline Architecture">
                            <div class="carousel-caption">
                                Simple Pipeline Architecture
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/visualizer/directories.png" alt"Directory Select Dialog">
                            <div class="carousel-caption">
                                Directory Select Dialog
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/visualizer/finder.png" alt"Project With a God Object Class">
                            <div class="carousel-caption">
                                Project With a God Object Class
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/visualizer/library.png" alt"Project With High Coupling">
                            <div class="carousel-caption">
                                Project With High Coupling
                            </div>
                        </div>

                        <div class="item">
                            <img src="assets/images/visualizer/visualizer.png" alt"Project With Acceptable Class Sizes Cohesion, and Coupling">
                            <div class="carousel-caption">
                                Project With Acceptable Class Sizes, Cohesion, and Coupling
                            </div>
                        </div>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-bottom:0">Close</button>
            </div>
        </div>
    </div>
</div>
<!--===============================================================-->