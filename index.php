
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <title>Kieran Harrison | ePortfolio</title>
    <!-- CSS -->
    <!--===============================================================-->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/theme.css" rel="stylesheet">
    <link href="assets/css/magnific-popup.css" rel="stylesheet">
    <link href="assets/css/styles.css" rel="stylesheet">
    <link href="assets/css/animation.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

  <!-- NAVBAR-->
  <!--===============================================================-->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" data-scroll href="#home"><img src="assets/images/logo.png" alt="logo"></a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a data-scroll href="#about">About Me</a></li>
            <li><a data-scroll href="#skills">Skills</a></li>
            <li><a data-scroll href="#portfolio">Portfolio</a></li>
            <li><a data-scroll href="#contact"><span class="contact">Contact</span></a></li>
          </ul>
        </div>
      </div>
    </div>

    <!-- INTRO-->
    <!--===============================================================-->
    <div class="bg-intro" id="home">
      <div class="layer-intro">
        <div class="container">
          <div class="row">
            <div class="col-sm-12 expandOpen delay">
              <h1 class="text-center">Hi ! <br /> I'm Kieran Harrison, <br /> a Software and Web Developer</h1>
              <br />
              <div class="row text-center no-padding">  
                  <img src="assets/images/portrait.jpg" alt="portrait" class="circle"/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="arrow-down"></div>

    <!-- ABOUT ME-->
    <!--===============================================================-->
    <div class="bg-about" id="about">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="title">ABOUT ME</h2>
                    <hr />
                </div>
            </div>

            <div class="row margin-bottom">
                <div class="col-sm-6 margin-bottom-xs">
                    <div class="row">
                        <div class="col-xs-2">
                            <i class="fa fa-graduation-cap fa-4x"></i>
                        </div>
                        <div class="col-xs-10">
                            <h3>B.Sc. in Computer Science</h3>
                            <p>
                                I completed the first two years of my B.Sc. at the University of the Fraser Valley before transferring to the University of
                                British Columbia to complete it.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-xs-2">
                            <i class="fa fa-home fa-4x"></i>
                        </div>
                        <div class="col-xs-10">
                            <h3>Greater Vancouver Based</h3>
                            <p>
                                Born and raised in Langley and currently living in Surrey.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row margin-bottom">
                <div class="col-sm-6 margin-bottom-xs">
                    <div class="row">
                        <div class="col-xs-2">
                            <i class="fa fa-desktop fa-4x"></i>
                        </div>
                        <div class="col-xs-10">
                            <h3>Application Development</h3>
                            <p>
                                I am quite familiar with using Java for developing desktop and mobile applications. I have also dabbled in C 
                                a little bit. I feel most comfortable working on database and backend tasks but am more than willing to cover 
                                all aspects of development.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-xs-2">
                            <i class="fa fa-code fa-4x"></i>
                        </div>
                        <div class="col-xs-10">
                            <h3>Web Development</h3>
                            <p>
                                All aspects of web development,from the frontend to the backend to migrating to a server, are well within my
                                capabilites.  All my work has been with HTML/CSS, JavaScript, PHP, and SQL, though of course I'm always willing
                                to learn a new language.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row margin-bottom">
                <div class="col-sm-6 margin-bottom-xs">
                    <div class="row">
                        <div class="col-xs-2">
                            <i class="fa fa-file-text fa-4x"></i>
                        </div>
                        <div class="col-xs-10">
                            <h3>Document Creation</h3>
                            <p>
                                Experience writing professional looking design documents and wireframes, software requirements specifications, 
                                contracts, and other technical documents.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-xs-2">
                            <i class="fa fa-group fa-4x"></i>
                        </div>
                        <div class="col-xs-10">
                            <h3>Experience Leading</h3>
                            <p>
                                Throughout all my group projects I have played a prominent role in the collaboration process, even acting 
                                as Project Manager for a term long project. I'm always willing to help my fellow teammates with any problem 
                                I can assist with.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row margin-bottom">
                <div class="col-sm-6 margin-bottom-xs">
                    <div class="row">
                        <div class="col-xs-2">
                            <i class="fa fa-eye fa-4x"></i>
                        </div>
                        <div class="col-xs-10">
                            <h3>Attentive to Detail</h3>
                            <p>
                                Whether you're looking at visuals generated by my code, the code itself, or documentation, you'll no doubt notice my love of
                                organization, neatness, and detail.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-xs-2">
                            <i class="fa fa-book fa-4x"></i>
                        </div>
                        <div class="col-xs-10">
                            <h3>Interests</h3>
                            <p>
                                Apart from software development I enjoy reading massive novels, playing board games, being outdoors, playing video/computer 
                                games, and weightlifting. 
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- SKILLS-->
    <!--===============================================================-->
    <div class="bg-skills" id="skills">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="title">MY SKILLS</h2>
                    <hr />
                </div>
            </div>

            <div class="row">
                <!-- 0-20 = Novice; 21-40 = Beginner; 41-60 = Intermediate; 61-80 = Proficient; 81-100 = Expert -->
                <div class="col-sm-6">
                    <h3>PROGRAMMING LANGUAGES</h3>
                    
                    <br />

                    <h4>Java</h4>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%">
                            Proficient
                        </div>
                    </div>

                    <h4>C</h4>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                            Novice
                        </div>
                    </div>

                    <h4>PHP</h4>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="78" aria-valuemin="0" aria-valuemax="100" style="width: 78%">
                            Proficient
                        </div>
                    </div>

                    <h4>HTML5/CSS3</h4>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                            Proficient
                        </div>
                    </div>

                    <h4>Javascript</h4>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                            Beginner
                        </div>
                    </div>

                    <h4>SQL</h4>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: 65%">
                            Proficient
                        </div>
                    </div>
                </div>

                <!-- 0-20 = Novice; 21-40 = Beginner; 41-60 = Intermediate; 61-80 = Proficient; 81-100 = Expert -->
                <div class="col-sm-6">
                    <h3>FRAMEWORKS AND OTHER SKILLS</h3>
                    
                    <br />

                    <h4>Bootstrap</h4>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                            Proficient
                        </div>
                    </div>

                    <h4>Laravel</h4>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%">
                            Proficient
                        </div>
                    </div>

                    <h4>Project Management</h4>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100" style="width: 55%">
                            Intermediate
                        </div>
                    </div>

                    <h4>Document Writing</h4>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                            Intermediate
                        </div>
                    </div>

                    <h4>Client Interaction</h4>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="63" aria-valuemin="0" aria-valuemax="100" style="width: 63%">
                            Proficient
                        </div>
                    </div>
                </div>
            </div>
            
            <br />

            <div class="row">
                <div class="col-md-8 col-md-offset-2">    
                    *Skill levels: Novice, Beginner, Intermediate, Proficient, Expert. Scale based off of estimated typical Junior/Intermediate Developer 
                    skill levels.
                </div>
            </div>
        </div>
    </div>

    <!-- PORTFOLIO-->
    <!--===============================================================-->
    <div class="bg-portfolio" id="portfolio">
      <div class="container">
        <div class="row">
          <!-- Title is different based on content -->
          <div class="col-sm-12">
            <?php
              if (!isset($_GET['content']))
                echo '<h2 class="title">LATEST WORK</h2>';
              else if($_GET['content'] == 'flex')
                echo '<h2 class="title">FLEX</h2>';
              else if($_GET['content'] == 'cbel')
                echo '<h2 class="title">CBEL TRACKER</h2>';
              else if($_GET['content'] == 'visualizer')
                echo '<h2 class="title">ROBOTANISM CODE VISUALIZER</h2>';
              else if($_GET['content'] == 'library')
                echo '<h2 class="title">MOCK LIBRARY</h2>';
            ?>
            <hr>
          </div>
        </div>
        <!-- Load content into portfolio section -->
        <div class="row row-portfolio">
            <?php
                if(isSet($_GET['content'])){
                    include('html/' . $_GET['content'] . '.php'); 
                }
                else{
                    include('html/portfolio.html');
                }
            ?>
        </div>
      </div>
    </div>

    <!-- CONTACT-->
    <!--===============================================================-->
    <div class="bg-contact" id="contact">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 text-center">
            <h3>Feel free to contact me for your next project!</h3>
          </div>
        </div>
      </div>
    </div>

    <!-- FOOTER-->
    <!--===============================================================-->
    <div class="bg-footer">
        <div class="container">
            <div class="row text-center">
                <div class="col-sm-12">
                    <a href="mailto:kieran.harrison@alumni.ubc.ca" class="mail" target="_blank">  
                        <i class="fa fa-envelope" data-toggle="tooltip" data-placement="top" title="Email"></i>
                    </a>
                    <a href="assets/documents/resume.pdf" target="_blank">  
                        <i class="fa fa-file-pdf-o" data-toggle="tooltip" data-placement="top" title="Resume"></i>
                    </a>
                    <a href="http://ca.linkedin.com/in/kieranharrison" target="_blank">
                        <i class="fa fa-linkedin" data-toggle="tooltip" data-placement="top" title="LinkedIn"></i>
                    </a>
                    <a href="https://bitbucket.org/kharrison1421" target="_blank">
                        <i class="fa fa-bitbucket" data-toggle="tooltip" data-placement="top" title="Bitbucket"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!-- FOOTER-BOTTOM-->
    <!--===============================================================-->
    <div class="bg-footer-bottom">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <p class="copyright">2015 &copy; Kieran Harrison - All Rights Reserved</p>
          </div>
          <div class="col-sm-6">
            <ul class="list-inline pull-right">
              <li class="border-right"><a href="#home" data-scroll>Top</a></li>
              <li class="border-right"><a href="#about" data-scroll>About Me</a></li>
              <li class="border-right"><a href="#skills" data-scroll>Skills</a></li>
              <li><a href="#portfolio" data-scroll>Portfolio</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <!-- JAVASCRIPT-->
    <!--===============================================================-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.magnific-popup.min.js"></script>
    <script src="assets/js/smooth-scroll.js"></script>
    <script src="assets/js/jRespond.min.js"></script>
    <script src="assets/js/numo.js"></script>
    <script src="assets/js/scripts.js"></script>
  </body>
</html>